#include <stdio.h>

struct Infos
{
    int number;
    int value;
};
typedef struct Infos Infos;

int sumEvens(Infos p[], int size);

int main()
{
    const char * str = "End!";

    int size = 4;
    Infos p[size];
    for (int i = 0; i < size; ++i)
    {
        p[i].number = i + 1;
        p[i].value = i + 1000;
    }

    if (sumEvens(p, size) < 256)
    {
        printf("\n");
    }

    printf("%s\n", str);
    return 0;
}

int sumEvens(Infos p[], int size)
{
    int result = 0;

    for (int i = 0; i < size; ++i)
    {
        if (p[i].number % 2 == 0)
        {
            result += p[i].value;
        }
    }

    return result;
}
