W pliku main.c jest kod programu, który będziemy analizować.

Aby go zbudować, należy skorzystać z make'a.
Odpowiednie komendy znajdują się w pliku Makefile.

Sposób użycia:
// Należy przejśc do folderu first i w nim wykonać komendę:
$ make

// W pliku Makefile jest opisanych kilka reguł dla tworzenia plików - 
// zostaną one w póżniejszym czasie rozszerzone. Kompilacja konkretnej reguły:
$ make asmFull

// Czyszczenie repo:
$ make clean
