#include <stdio.h>

void copyTable(int p[], int size);
int sumEvens(int p[], int size);

int main()
{
    const char * str = "Hi, world!";

    int size = 5;
    int p[size];
    for (int i = 0; i < size; ++i)
    {
        p[i] = i + 1;
    }

    if (sumEvens(p, size) < 256)
    {
        copyTable(p, size);
        printf("\n");
    }

    printf("%s\n", str);
    return 0;
}

void copyTable(int p[], int size)
{
    int temp[size];

    for (int i = 0; i < size; i++)
    {
        temp[i] = p[i];
    }
    
    for (int i = 0; i < size; ++i)
    {
        printf("%d ", temp[i]);
    }
}

int sumEvens(int p[], int size)
{
    int result = 0;

    for (int i = 0; i < size; ++i)
    {
        if (p[i] % 2 == 0)
        {
            result += p[i];
        }
    }

    return result;
}
